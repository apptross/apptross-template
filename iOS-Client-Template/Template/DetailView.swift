//
//  BookDetailView.swift
//  
//
//  Created by tana on 2016/04/23.
//
//

import Foundation
import UIKit

class DetailView :UIView{
    
//    lazy private var titleLabel:UILabel = self.createTitleLabel()
////    lazy private var submitButton:UIButton = self.createSubmitButton()
//    
//    var title:String? {
//        get {
//            return titleLabel.text
//        }
//        set(title) {
//            titleLabel.text = title
//            self.setNeedsLayout()
//        }
//    }
//    
//    override
//    init(frame: CGRect) {
//        super.init(frame: frame)
//        self.backgroundColor = UIColor.blueColor()
////        commonInit()
//    }
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//        
//    }
//    
//    private func commonInit() {
//        self.backgroundColor = UIColor.grayColor()
//        //UIColor(white: 0.9, alpha: 1.0)
//        self.addSubview(titleLabel)
//
//    }
//    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        layoutTitleLabel()
//    }
//    
//    private func createTitleLabel() -> UILabel {
//        let label = UILabel(frame: CGRectZero)
//        label.font = UIFont.systemFontOfSize(12)
//        label.textColor = UIColor.whiteColor()
//        return label
//    }
//    
//    private func layoutTitleLabel() {
//        titleLabel.sizeToFit()
//        titleLabel.center.x = self.frame.size.width/2
//        titleLabel.frame.origin.y = 100
//    }
    
    @IBOutlet var mountainLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var mountainImage: UIImageView!
    
    class func instance() -> DetailView {
        return UINib(nibName: "DetailView", bundle: nil).instantiateWithOwner(self, options: nil)[0] as! DetailView
    }
    
}