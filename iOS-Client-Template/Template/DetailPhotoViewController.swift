//
//  ViewController.swift
//  Template
//
//  Created by tana on 2016/04/21.
//  Copyright © 2016年 com.tanaike. All rights reserved.
//

import UIKit
import MapKit

class DetailPhotoViewController: UIViewController {
    var mPhoto: Photo?
    
    var mScrollView: UIScrollView!
    var mStackView: UIStackView!
    
    let PHOTO_W: CGFloat = 375
    let PHOTO_H: CGFloat = 180
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.blackColor()
//
//        let detailView = DetailView.instance()
//        detailView.mountainLabel.text = mPhoto?.mountain_name
//        detailView.descriptionLabel.text = mPhoto?.mountain_descripton
//        
////        var imgView = UIImageView()
//        do {
//            
//            let url = NSURL(string: mPhoto!.url!);
//            let imgData = try NSData(contentsOfURL:url!,options: NSDataReadingOptions.DataReadingMappedIfSafe)
//            let img = UIImage(data:imgData);
//            detailView.mountainImage = UIImageView(image:img)
//            detailView.mountainImage.frame = CGRectMake(0, 0, 400, 200);
//            detailView.mountainImage.heightAnchor.constraintEqualToConstant(200.0).active = true
//            detailView.mountainImage.widthAnchor.constraintEqualToConstant(400.0).active = true
//            
//            
//        } catch {
//            print("Error: can't create image.")
//        }
////         detailView.mountainImage = imgView
//        
////        let detailView = DetailView()
//        self.view.addSubview(detailView)
//        
        /*
        */
        let dummyLabel = UILabel()
        dummyLabel.text  = ""
        dummyLabel.heightAnchor.constraintEqualToConstant(60.0).active = true
        
        let moutainNameLabel = UILabel()
        moutainNameLabel.backgroundColor = UIColor.grayColor()
        moutainNameLabel.widthAnchor.constraintEqualToConstant(self.view.frame.width).active = true
        moutainNameLabel.heightAnchor.constraintEqualToConstant(50.0).active = true
        moutainNameLabel.text  = mPhoto?.mountain_name
        moutainNameLabel.textAlignment = .Center
        moutainNameLabel.font = UIFont.systemFontOfSize(30)
        
        var imgView = UIImageView()
        do {
            
            let url = NSURL(string: mPhoto!.url!);
            let imgData = try NSData(contentsOfURL:url!,options: NSDataReadingOptions.DataReadingMappedIfSafe)
            let img = UIImage(data:imgData);
            imgView = UIImageView(image:img);
//            imgView.frame = CGRectMake(0, 0, 400, 200);
            imgView.heightAnchor.constraintEqualToConstant(PHOTO_H).active = true
            imgView.widthAnchor.constraintEqualToConstant(PHOTO_W).active = true
            
        } catch {
            print("Error: can't create image.")
        }
        
        
        //Text Label
        let textLabel = UILabel()
        textLabel.backgroundColor = UIColor.grayColor()
        textLabel.widthAnchor.constraintEqualToConstant(self.view.frame.width).active = true
//        textLabel.heightAnchor.constraintEqualToConstant(200.0).active = true
        textLabel.text  = mPhoto?.mountain_descripton
        textLabel.numberOfLines = 0
        textLabel.textAlignment = .Left
        textLabel.sizeToFit()
        
        let button = UIButton()
        let buttonStr = mPhoto!.mountain_name! + "の詳細を表示"
        button.setTitle(buttonStr, forState: .Normal)
        button.setTitleColor(UIColor.blueColor(), forState: .Normal)
        button.setTitleColor(UIColor.blackColor(), forState: .Highlighted)
        button.addTarget(self, action: "showWikiSite", forControlEvents:.TouchUpInside)
//        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.backgroundColor = UIColor.grayColor()
        button.widthAnchor.constraintEqualToConstant(self.view.frame.width).active = true
        
//        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(35.68154,139.752498)
        let latitudeString = mPhoto?.latitude
        let longitudeString = mPhoto?.longtitude
        let latitude = (latitudeString! as NSString).doubleValue
        let longitude = (longitudeString! as NSString).doubleValue
        
        let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)

        let mapView = MKMapView()
        mapView.widthAnchor.constraintEqualToConstant(self.view.frame.width).active = true
        mapView.heightAnchor.constraintEqualToConstant(PHOTO_H).active = true
        mapView.setCenterCoordinate(location,animated:true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        mapView.addAnnotation(annotation)
        
        var region:MKCoordinateRegion = mapView.region
        region.center = location
        region.span.latitudeDelta = 0.02
        region.span.longitudeDelta = 0.02
        
        //Stack View
        let stackView   = UIStackView()
        stackView.axis  = UILayoutConstraintAxis.Vertical
        stackView.distribution  = UIStackViewDistribution.EqualSpacing
        stackView.alignment = UIStackViewAlignment.Center
//        stackView.alignment = UIStackViewAlignment.FirstBaseline
        stackView.spacing   = 5.0
        
//        stackView.addArrangedSubview(dummyLabel)
        stackView.addArrangedSubview(moutainNameLabel)
        stackView.addArrangedSubview(imgView)
        stackView.addArrangedSubview(textLabel)
        stackView.addArrangedSubview(mapView)
        stackView.addArrangedSubview(button)
        stackView.translatesAutoresizingMaskIntoConstraints = false;

//        stackView.centerXAnchor.constraintEqualToAnchor(self.view.centerXAnchor).active = true

//        self.view.addSubview(stackView)
        mStackView = stackView
        
        mScrollView = UIScrollView()
        mScrollView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(mScrollView)

        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[mScrollView]|", options: .AlignAllCenterX, metrics: nil, views: ["mScrollView": mScrollView]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[mScrollView]|", options: .AlignAllCenterX, metrics: nil, views: ["mScrollView": mScrollView]))

        mScrollView.addSubview(mStackView)

        mScrollView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[mStackView]|", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: ["mStackView": mStackView]))
        mScrollView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[mStackView]", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: ["mStackView": mStackView]))

////        //Constraints
//        stackView.centerXAnchor.constraintEqualToAnchor(self.view.centerXAnchor).active = true
////        stackView.centerYAnchor.constraintEqualToAnchor(self.view.centerYAnchor).active = true
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mScrollView.contentSize = CGSize(width: mStackView.frame.width, height: mStackView.frame.height)
    }
    
    func showWikiSite () {
        print("showWikiSite start")
        
        let webViewController = WebViewController()
        let baseWikiUrl = "https://en.wikipedia.org/wiki/"
        let targetUrl = baseWikiUrl + (mPhoto?.mountain_name)!
//        let targetUrl = (mPhoto?.wikiurl)!
        print("targetUrl : \(targetUrl)")
        let encodedString = targetUrl.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())

        webViewController.loadUrl(encodedString!)
        self.navigationController?.pushViewController(webViewController, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPhoto (photo: Photo) { // fixme PhotoManagerを有効にして、PhotoIdを渡すだけにすべき
        mPhoto = photo
    }

    
}


