//
//  SimilarListResponse.swift
//  Template
//
//  Created by tana on 2016/04/23.
//  Copyright © 2016年 com.tanaike. All rights reserved.
//

import ObjectMapper

class SimilarListResponse: Mappable {
    var status: String?
    var list: [Photo]?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        list <- map["list"]
    }
}

