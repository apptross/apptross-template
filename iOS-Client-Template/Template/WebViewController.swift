//
//  WebViewController.swift
//  Template
//
//  Created by tana on 2016/04/24.
//  Copyright © 2016年 com.tanaike. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, UIWebViewDelegate, WKNavigationDelegate{
    
    private var mWebView: UIWebView!
    private var mProgressView: UIProgressView!
    /*
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let progressView = UIProgressView(frame: CGRectMake(0, self.navigationController!.navigationBar.frame.size.height - 2, self.view.frame.size.width, 10))
//        progressView.progressViewStyle = .Bar
        mProgressView = UIProgressView(frame: CGRectMake(0, self.navigationController!.navigationBar.frame.size.height - 2, self.view.frame.size.width, 10))
        mProgressView.progressViewStyle = .Bar

        self.navigationController?.navigationBar.addSubview(mProgressView)
    }
    
    override func viewWillAppear(animated: Bool){
        super.viewWillAppear(animated)
        mWebView.addObserver(self, forKeyPath: "estimatedProgress", options: .New, context: nil)
        mWebView.addObserver(self, forKeyPath: "loading", options: .New, context: nil)
    }
    
    override func viewWillDisappear(animated: Bool){
        mWebView.removeObserver(self, forKeyPath: "estimatedProgress")
        mWebView.removeObserver(self, forKeyPath: "loading")
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
//        if keyPath == "estimatedProgress"{
//            mProgressView.setProgress(Float(mWebView.e), animated: true)
//        }else if (kyPath == "loading"{
//            UIApplication.sharedApplication().networkActivityIndicatorVisible = mWebView.loading
//            if (mWebView.loading) {
//                self.navigationController?.navigationBar.addSubview(mProgressView)
//            } else {
//                mProgressView.removeFromSuperview()
//            }
//        }
    }
    
    */
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("webViewDidFinishLoad")
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        print("webViewDidStartLoad")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadUrl(urlString:String) {
        mWebView = UIWebView()
        mWebView.delegate = self
        
        mWebView.frame = self.view.bounds
        self.view.addSubview(mWebView)
        
        let url: NSURL = NSURL(string: urlString)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        mWebView.loadRequest(request)
    }
    
}
