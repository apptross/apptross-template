
import UIKit

class SimilarListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var mCollectionView:UICollectionView!
    var mApiClient: ApiClient? = nil
    var mPhotoList: [Photo]? = []
    
    var mPhotoId: String? = ""
    
    var mRefreshBtn: UIBarButtonItem!
    let PHOTO_W: CGFloat = 375
    let PHOTO_H: CGFloat = 180

    override
    func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "テーブルビュー"
        
        self.view.backgroundColor = UIColor.cyanColor()
        
        mRefreshBtn = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "onClickRefresh")
        self.navigationItem.rightBarButtonItem = mRefreshBtn
        
        // レイアウト作成
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .Vertical
        flowLayout.minimumInteritemSpacing = 5.0
        flowLayout.minimumLineSpacing = 5.0
        flowLayout.itemSize = CGSizeMake(PHOTO_W, PHOTO_H)
        
        // コレクションビュー作成
        mCollectionView = UICollectionView(frame: view.frame, collectionViewLayout: flowLayout)
        mCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        mCollectionView.dataSource = self
        mCollectionView.delegate = self
        view.addSubview(mCollectionView)

    }
    
    override
    func viewWillAppear(animated: Bool) {
        mApiClient = ApiClient.sharedInstance

        mApiClient?.requestSimilarList(mPhotoId!)(callback:{(success:Bool, photoList:[Photo]) in
            self.mPhotoList = photoList
            self.mCollectionView.reloadData()
        })
    }

    func onClickRefresh() {
        print("onClickRefresh")
        
        mApiClient?.requestSelectList({( success:Bool, photoList:[Photo]) in
            
            self.mPhotoList = photoList
            self.mCollectionView.reloadData()
            
        })
        
    }

    override
    func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("selected %d", indexPath.row);
        
        let detailPhotoViewController = DetailPhotoViewController()
        let photo = mPhotoList![indexPath.row]
        detailPhotoViewController.setPhoto(photo)
        self.navigationController?.pushViewController(detailPhotoViewController, animated: true)
        
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mPhotoList!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell
        cell.backgroundColor = UIColor.redColor()
        
        do {
            
            let photo = self.mPhotoList![indexPath.row]
//            var photoUrlString = photo.url!
//            let str:String = "abcdefghi"
//            photoUrlString = photoUrlString.stringByReplacingOccurrencesOfString("_s.jpg", withString: "_n.jpg", options: nil, range: nil)
            
            
            let url = NSURL(string: photo.url!);
            let imgData = try NSData(contentsOfURL:url!,options: NSDataReadingOptions.DataReadingMappedIfSafe)
            var img = UIImage(data:imgData);
            
            img = Util.cropThumbnailImage(img!, w: Int(PHOTO_W), h: Int(PHOTO_H))
            
            let imgView = UIImageView(image:img);
            imgView.frame = CGRectMake(0, 0, PHOTO_W, PHOTO_H);
            cell.addSubview(imgView);
            
            let label = UILabel(frame: CGRectMake(0, 00, PHOTO_W, 20))
            label.text = photo.mountain_name
            label.backgroundColor = UIColor(red:0.6,green:0.0,blue:0.0,alpha:0.2)
            label.textColor = UIColor.whiteColor()
            cell.addSubview(label)
            
            
        } catch {
            print("Error: can't create image.")
        }
        
        return cell
    }
    
    func setPhotoId (photoId: String) {
        mPhotoId = photoId
    }
    
    
}