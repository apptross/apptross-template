//
//  Photos.swift
//  Template
//
//  Created by tana on 2016/04/23.
//  Copyright © 2016年 com.tanaike. All rights reserved.
//

import ObjectMapper

class Photo: Mappable {
    var id: String?
    var url: String?
    var category_id: String?
    var category_label: String?
    var photo_title: String?
    var latitude: String?
    var longtitude: String?
    var mountain_name: String?
    var mountain_descripton: String?
    var wikiurl: String?
    
    required init?(_ map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        url <- map["url"]
        category_id <- map["category_id"]
        category_label <- map["category_label"]
        photo_title <- map["photo_title"]
        latitude <- map["latitude"]
        longtitude <- map["longtitude"]
        mountain_name <- map["mountain_name"]
        wikiurl <- map["wikiurl"]
        mountain_descripton <- map["mountain_descripton"]
    }
}


