//
//  MountPickClient.swift
//  Template
//
//  Created by tana on 2016/04/23.
//  Copyright © 2016年 com.tanaike. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class ApiClient: NSObject{
    
    private let ApiBaseUrl : String = "http://localhost:8888/SpaceAppMoutainPick/testdata/"
    private let SelectListPath : String = "SelectList.php"
    private let SimilarListPath : String = "SimilarList.php"
//    private let ApiBaseUrl : String = "http://172.20.10.3:8000/"
//    private let ApiBaseUrl : String = "http://127.0.0.1:8000/"
//    private let SelectListPath : String = "SelectList"
//    private let SimilarListPath : String = "SimilarList"
    
    static let sharedInstance = ApiClient()
    
    override
    private init() {
        
    }

    func requestSelectList(callback:(Bool, [Photo]) -> Void) -> Void{
        
        let requestUrl:String = ApiBaseUrl + SelectListPath
        
        Alamofire.request(.GET, requestUrl)
            .responseObject { (response: Response<PhotoListResponse, NSError>) in
                print("response.request :\(response.request) ")
                print("response.response :\(response.response) ")
                print("response.result :\(response.result) ")
                
                switch response.result {
                case .Success:
                    print("Validation Successful")
                    let PhotoListResponse = response.result.value
                    print(PhotoListResponse?.status)
                    
                    if (PhotoListResponse?.status == "ok") {
                        callback(true, (PhotoListResponse?.list)!)
                    } else {
                        callback(false, (PhotoListResponse?.list)!)
                    }

                case .Failure(let error):
                    print(error)
                    callback(false, [])
                }
        }
        
    }
    

    func requestSimilarList(photoId:String) (callback:(Bool, [Photo]) -> Void) -> Void{
        
        let requestUrl:String = ApiBaseUrl + SimilarListPath
        
        Alamofire.request(.GET, requestUrl, parameters: ["photo_id": photoId])
            .responseObject { (response: Response<PhotoListResponse, NSError>) in
                print("response.request :\(response.request) ")
                print("response.response :\(response.response) ")
                print("response.result :\(response.result) ")
                
                switch response.result {
                case .Success:
                    print("Validation Successful")
                    let PhotoListResponse = response.result.value
                    print(PhotoListResponse?.status)
                    
                    if (PhotoListResponse?.status == "ok") {
                        callback(true, (PhotoListResponse?.list)!)
                    } else {
                        callback(false, (PhotoListResponse?.list)!)
                    }
                    
                case .Failure(let error):
                    print(error)
                    callback(false, [])
                }
        }
        
    }
}
