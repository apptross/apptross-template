
import UIKit
import AlamofireImage

import CocoaLumberjack


class HomeListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    var mCollectionView:UICollectionView!
    var mApiClient: ApiClient? = nil
    var mPhotoList: [Photo]? = []
    
    var mRefreshBtn: UIBarButtonItem!

    let PHOTO_SIZE: CGFloat = 180
    
    override
    func viewDidLoad() {
        super.viewDidLoad()

        DDLogDebug("didUpdateToLocation")
        
        self.title = "Template"
        
        mRefreshBtn = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: "onClickRefresh")
        self.navigationItem.rightBarButtonItem = mRefreshBtn
        
        mApiClient = ApiClient.sharedInstance
        
        // レイアウト作成
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .Vertical
        flowLayout.minimumInteritemSpacing = 2.0
        flowLayout.minimumLineSpacing = 15.0
//        flowLayout.itemSize = CGSizeMake(200, 200)
        flowLayout.itemSize = CGSizeMake(PHOTO_SIZE, PHOTO_SIZE)
        
        // コレクションビュー作成
        mCollectionView = UICollectionView(frame: view.frame, collectionViewLayout: flowLayout)
        mCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        mCollectionView.dataSource = self
        mCollectionView.delegate = self
        view.addSubview(mCollectionView)
        
        mApiClient?.requestSelectList({( success:Bool, photoList:[Photo]) in
            
            self.mPhotoList = photoList
            self.mCollectionView.reloadData()
       
        })
        
    }
    
    func onClickRefresh() {
        DDLogDebug("onClickRefresh")
        
        mApiClient?.requestSelectList({( success:Bool, photoList:[Photo]) in
            
            self.mPhotoList = photoList
            self.mCollectionView.reloadData()
            
        })
        
    }
    
    override
    func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("selected %d", indexPath.row);
        
        let similarListViewController = SimilarListViewController()
        let photoId = mPhotoList![indexPath.row].id
        similarListViewController.setPhotoId(photoId!)
        self.navigationController?.pushViewController(similarListViewController, animated: true)
        
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mPhotoList!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as UICollectionViewCell
        cell.backgroundColor = UIColor.redColor()
        
        
        let photo = self.mPhotoList![indexPath.row]
        let url = NSURL(string: photo.url!)!

        let frame = CGRectMake(0, 0, PHOTO_SIZE, PHOTO_SIZE);
        let imgView = UIImageView(frame: frame)
        
        imgView.af_setImageWithURL(url)
        
        
        cell.addSubview(imgView);
        
        let label = UILabel(frame: CGRectMake(0, 0, PHOTO_SIZE, 20))

        label.text = photo.category_label
        label.backgroundColor = UIColor(red:0.6,green:0.0,blue:0.0,alpha:0.2)
        label.textColor = UIColor.whiteColor()
        cell.addSubview(label)
        
        return cell
    }
    
    
}